Closes ___ (e.g. #230)

## What does this MR do?

(briefly describe what this MR is about)

## How confident are you it won't break things if deployed?

(be honest!) 

## Links to related issues

Any relevant links (issues, documentation, slack discussions).

## How to test

Steps a reviewer can take to verify that this MR does what it says it does e.g.

1. Checkout branch locally
2. Login as foodsaver
3. ...

## Screenshots (if applicable)

Any relevant screenshots if this is a design / frontend change

## Checklist

- [ ] added a test, or explain why one is not needed/possible...
- [ ] no unrelated changes
- [ ] asked someone for a code review 
- [ ] set a "for:" label to indicate who will be affected by this change
- [ ] use "state:" labels to track this MR's state until it was beta tested 
- [ ] added an entry to CHANGELOG.md
- [ ] add a short text that can be used in the release notes
- [ ] Once your MR has been merged, you are responsible to create a testing issue in [Beta Testing Repo](https://gitlab.com/foodsharing-dev/foodsharing-beta-testing):  
     *  Consider writing a detailed description **in German**.  
     *  Describe in a few sentences, what should be tested from a **user perspective**.   
     *  Also mention different settings (e.g. **different browsers**, roles, ...).   how this change can be tested.   
     *  Be aware, that also **non technical** people should understand.  

## Release notes text

(A short text that will appear in the release notes and describes the change for non-technical people. Not always necessary, e.g. not for refactoring.)